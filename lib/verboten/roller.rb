# coding: utf-8
# frozen_string_literal: true

# Copyright 2018 Richard Davis, Rod Eric Joseph
#
# This file is part of verboten.
#
# verboten is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# verboten is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with verboten.  If not, see <http://www.gnu.org/licenses/>.

require 'securerandom'

module Verboten
  ##
  # = Roller
  # Author::    Rod Eric Joseph
  # Copyright:: Copyright 2019 Mushaka Solutions
  # License::   GNU Public License 3
  #
  # Roll fair dice, flip a coin.
  module Roller
    ##
    # Rolls an n-sided die, where n is args[0]
    def roll_die(args)
      return 'Please provide a valid maximum number for the roll.' unless not(args[0].nil?) and args[0].match? /^\d*[1-9]\d*$/
      begin
        n = args[0].to_i
        puts n
        result = 1 + SecureRandom.random_number(n)

        if result.to_s[0] == '8' or result == 18
          article = 'an'
        else
          article = 'a'
        end
        "You rolled #{article} #{result}! 😵"
      rescue
        'Please provide a valid maximum number for the roll.'
      end
    end

    ##
    # Rolls a die, returning either heads or tails.
    def flip_coin
      result = SecureRandom.random_number(2)
      if result == 0
        return 'Heads.'
      else
        return 'Tails.'
      end
    end
  end
end
        
