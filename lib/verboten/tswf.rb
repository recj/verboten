# frozen_string_literal: true

# Copyright 2018 Richard Davis
#
# This file is part of verboten.
#
# verboten is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# verboten is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with verboten.  If not, see <http://www.gnu.org/licenses/>.

require 'net/https'
require 'uri'

module Verboten
  ##
  # = TSWF
  # Author::    Richard Davis
  # Copyright:: Copyright 2018 Mushaka Solutions
  # License::   GNU Public License 3
  #
  # Subroutine to add and skip songs in the tswf queue.
  module TSWF
    API = 'https://hackerchan.org/api'
    PLAY_ENDPOINT = '/submit?song='
    SKIP_ENDPOINT = '/skip?username='

    ##
    # Validates that the given url meets criteria
    def is_valid_url?(url)
      %r{https?:\/\/[\S]+[\.][\S]{2,}\/[\S]+} =~ url
    end

    ##
    # Queues a song up at tsfw
    def send_song_to_tswf(args)
      return 'You did not provide a song to add to the queue. 😕' if args[0].nil?
      return 'You entered an invalid URL. 🤨' unless is_valid_url?(args[0]) || args[0].casecmp('despacito').zero?

      if args[0].casecmp('despacito').zero?
        link = API+PLAY_ENDPOINT+'https://invidio.us/watch?v=kJQP7kiw5Fk'
      else
        link = API+PLAY_ENDPOINT+args[0]
      end

      uri = URI.parse(link)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      if response.code == '200'
        'Song successfully added to the queue. 🎉'
      else
        'The server rejected this song. 😖'
      end
    end

    ##
    # Skips the currently playing song
    def skip_current_song(username)
      link = API+SKIP_ENDPOINT+username
      uri = URI.parse(link)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      if response.code == '200'
        'Your vote has been recorded. 🎉'
      elsif response.code == '403'
        "Nice try, #{username}! You already voted! 👮"
      elsif response.code == '400'
        'I did an oopsie whoopsie; tell d3d1rty to fix me. 😖'
      elsif response.code == '404'
        'Hmmm, I think the endpoint might have moved. 🧐'
      elsif response.code == '405'
        'There is nothing playing right now. 😩'
      else
        'Something went wrong! 😵'
      end
    end
  end
end
